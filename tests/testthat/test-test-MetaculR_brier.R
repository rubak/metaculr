testthat::skip_on_cran()
testthat::skip_on_ci()
testthat::skip_on_covr()

testthat::test_that("MetaculR_questions_offset", {
  httptest::capture_requests({
    Metaculus_response_login <- MetaculR_login()

    questions_resolved_offset <-
      MetaculR_questions(
        order_by = "resolve_time",
        offset = 1990,
        pages = 2)
  })

  testthat::expect_true(
    9939 %in% questions_resolved_offset[[2]]$results$id
  )
})





testthat::test_that("MetaculR_analysis_binary_resolved", {
  httptest::capture_requests({
    Metaculus_response_login <- MetaculR_login()

    questions_resolved_offset <-
      MetaculR_questions(
        order_by = "resolve_time",
        offset = 1990,
        pages = 2)
  })

  questions_resolved_analysis_binary_offset <-
    MetaculR_analysis_binary_resolved(
      questions_resolved_offset)

  testthat::expect_equal(
    questions_resolved_analysis_binary_offset %>%
      dplyr::filter(id %in% c(9933, 9939, 10282, 9790)) %>%
      dplyr::distinct(id) %>%
      dplyr::ungroup() %>%
      dplyr::tally() %>%
      dplyr::pull(),
    4
  )

  testthat::expect_equal(
    nrow(questions_resolved_analysis_binary_offset),
    1718)

  testthat::expect_equal(
    ncol(questions_resolved_analysis_binary_offset),
    60)
})





testthat::test_that("MetaculR_brier_me_resolve", {
  httptest::capture_requests({
    Metaculus_response_login <- MetaculR_login()

    questions_resolved_offset <-
      MetaculR_questions(
        order_by = "resolve_time",
        offset = 1990,
        pages = 2)
  })

  questions_resolved_analysis_binary_offset <-
    MetaculR_analysis_binary_resolved(
      questions_resolved_offset)

  brier_resolved_offset_defaults <-
    MetaculR_brier(
      questions_resolved_analysis_binary_offset %>%
        dplyr::filter(id %in% c(9933, 9939, 10282, 9790))) ### Track Record: all times, 2022-02-01 - 2022-04-15; resolve time, 2022-03-15 - 2022-04-15; close time, 2022-03-22 - 2022-04-11 [9609 has no "Me" prediction]

  testthat::expect_equal(
    round(brier_resolved_offset_defaults$brier_me$bs, 3),
    0.100) # Metaculus.com: 0.1
  testthat::expect_equal(
    round(brier_resolved_offset_defaults$brier_Metaculus$bs, 3),
    0.014) # Metaculus.com: 0.014
  testthat::expect_equal(
    round(brier_resolved_offset_defaults$brier_community$bs, 3),
    0.008) # !!! Metaculus.com: 0.009
})





testthat::test_that("MetaculR_brier_notme_resolve", {
  httptest::capture_requests({
    Metaculus_response_login <- MetaculR_login()

    questions_resolved_offset <-
      MetaculR_questions(
        order_by = "resolve_time",
        offset = 1990,
        pages = 2)
  })

  questions_resolved_analysis_binary_offset <-
    MetaculR_analysis_binary_resolved(
      questions_resolved_offset)

  brier_resolved_offset_notme <-
    MetaculR_brier(
      questions_resolved_analysis_binary_offset %>%
        dplyr::filter(id %in% c(9933, 9939, 10282, 9790)), ### Track Record: all times, 2022-02-01 - 2022-04-15; resolve time, 2022-03-15 - 2022-04-15; close time, 2022-03-22 - 2022-04-11 [9609 has no "Me" prediction]
      me = FALSE)

  testthat::expect_equal(
    brier_resolved_offset_notme$brier_me$bs,
    NA)
  testthat::expect_equal(
    round(brier_resolved_offset_notme$brier_Metaculus$bs, 3),
    0.014) # Metaculus.com: 0.014
  testthat::expect_equal(
    round(brier_resolved_offset_notme$brier_community$bs, 3),
    0.008) # Metaculus.com: 0.009
})





testthat::test_that("MetaculR_brier_me_close_question", {
  httptest::capture_requests({
    Metaculus_response_login <- MetaculR_login()

    questions_resolved_offset <-
      MetaculR_questions(
        order_by = "resolve_time",
        offset = 1990,
        pages = 2)
  })

  questions_resolved_analysis_binary_offset <-
    MetaculR_analysis_binary_resolved(
      questions_resolved_offset)

  brier_resolved_offset_close_question <-
    MetaculR_brier(
      questions_resolved_analysis_binary_offset %>%
        dplyr::filter(id %in% c(9933, 9939, 10282, 9790)), ### Track Record: all times, 2022-02-01 - 2022-04-15; resolve time, 2022-03-15 - 2022-04-15; close time, 2022-03-22 - 2022-04-11 [9609 has no "Me" prediction]
      time = "close",
      unit = "question")

  testthat::expect_equal(
    round(brier_resolved_offset_close_question$brier_me$bs, 3),
    0.084) # Metaculus.com: 0.1
  testthat::expect_equal(
    round(brier_resolved_offset_close_question$brier_Metaculus$bs, 3),
    0.068) # Metaculus.com: 0.015 (!!! Not equal to "resolve time"?)
  testthat::expect_equal(
    round(brier_resolved_offset_close_question$brier_community$bs, 3),
    0.071) # !!! Metaculus.com: 0.011 (!!! Not equal to "resolve time"?)
})





testthat::test_that("MetaculR_brier_notme_close_question", {
  httptest::capture_requests({
    Metaculus_response_login <- MetaculR_login()

    questions_resolved_offset <-
      MetaculR_questions(
        order_by = "resolve_time",
        offset = 1990,
        pages = 2)
  })

  questions_resolved_analysis_binary_offset <-
    MetaculR_analysis_binary_resolved(
      questions_resolved_offset)

  brier_resolved_offset_notme_close_question <-
    MetaculR_brier(
      questions_resolved_analysis_binary_offset %>%
        dplyr::filter(id %in% c(9933, 9939, 10282, 9790)), ### Track Record: all times, 2022-02-01 - 2022-04-15; resolve time, 2022-03-15 - 2022-04-15; close time, 2022-03-22 - 2022-04-11 [9609 has no "Me" prediction]
      me = FALSE,
      time = "close",
      unit = "question")

  testthat::expect_equal(
    brier_resolved_offset_notme_close_question$brier_me$bs,
    NA)
  testthat::expect_equal(
    round(brier_resolved_offset_notme_close_question$brier_Metaculus$bs, 3),
    0.079)
  testthat::expect_equal(
    round(brier_resolved_offset_notme_close_question$brier_community$bs, 3),
    0.087)
})





testthat::test_that("MetaculR_brier_me_all_question", {
  httptest::capture_requests({
    Metaculus_response_login <- MetaculR_login()

    questions_resolved_offset <-
      MetaculR_questions(
        order_by = "resolve_time",
        offset = 1990,
        pages = 2)
  })

  questions_resolved_analysis_binary_offset <-
    MetaculR_analysis_binary_resolved(
      questions_resolved_offset)

  brier_resolved_offset_all_question <-
    MetaculR_brier(
      questions_resolved_analysis_binary_offset %>%
        dplyr::filter(id %in% c(9933, 9939, 10282, 9790)), ### Track Record: all times, 2022-02-01 - 2022-04-15; resolve time, 2022-03-15 - 2022-04-15; close time, 2022-03-22 - 2022-04-11 [9609 has no "Me" prediction]
      time = "all",
      unit = "question")

  testthat::expect_equal(
    round(brier_resolved_offset_all_question$brier_me$bs, 3),
    0.061) # Metaculus.com: 0.124
  testthat::expect_equal(
    round(brier_resolved_offset_all_question$brier_Metaculus$bs, 3),
    0.045) # Metaculus.com: 0.081
  testthat::expect_equal(
    round(brier_resolved_offset_all_question$brier_community$bs, 3),
    0.046) # Metaculus.com: 0.078
})





testthat::test_that("MetaculR_brier_notme_all_question", {
  httptest::capture_requests({
    Metaculus_response_login <- MetaculR_login()

    questions_resolved_offset <-
      MetaculR_questions(
        order_by = "resolve_time",
        offset = 1990,
        pages = 2)
  })

  questions_resolved_analysis_binary_offset <-
    MetaculR_analysis_binary_resolved(
      questions_resolved_offset)

  brier_resolved_offset_notme_all_question <-
    MetaculR_brier(
      questions_resolved_analysis_binary_offset %>%
        dplyr::filter(id %in% c(9933, 9939, 10282, 9790)), ### Track Record: all times, 2022-02-01 - 2022-04-15; resolve time, 2022-03-15 - 2022-04-15; close time, 2022-03-22 - 2022-04-11 [9609 has no "Me" prediction]
      me = FALSE,
      time = "all",
      unit = "question")

  testthat::expect_equal(
    brier_resolved_offset_notme_all_question$brier_me$bs,
    NA)
  testthat::expect_equal(
    round(brier_resolved_offset_notme_all_question$brier_Metaculus$bs, 3),
    0.057)
  testthat::expect_equal(
    round(brier_resolved_offset_notme_all_question$brier_community$bs, 3),
    0.064)
})





testthat::test_that("MetaculR_brier_me_all_second", {
  httptest::capture_requests({
    Metaculus_response_login <- MetaculR_login()

    questions_resolved_offset <-
      MetaculR_questions(
        order_by = "resolve_time",
        offset = 1990,
        pages = 2)
  })

  questions_resolved_analysis_binary_offset <-
    MetaculR_analysis_binary_resolved(
      questions_resolved_offset)

  brier_resolved_offset_all_question <-
    MetaculR_brier(
      questions_resolved_analysis_binary_offset %>%
        dplyr::filter(id %in% c(9933, 9939, 10282, 9790)), ### Track Record: all times, 2022-02-01 - 2022-04-15; resolve time, 2022-03-15 - 2022-04-15; close time, 2022-03-22 - 2022-04-11 [9609 has no "Me" prediction]
      time = "all",
      unit = "second")

  testthat::expect_equal(
    round(brier_resolved_offset_all_question$brier_me$bs, 3),
    0.095)
  testthat::expect_equal(
    round(brier_resolved_offset_all_question$brier_Metaculus$bs, 3),
    0.063)
  testthat::expect_equal(
    round(brier_resolved_offset_all_question$brier_community$bs, 3),
    0.057)
})





testthat::test_that("MetaculR_brier_notme_all_second", {
  httptest::capture_requests({
    Metaculus_response_login <- MetaculR_login()

    questions_resolved_offset <-
      MetaculR_questions(
        order_by = "resolve_time",
        offset = 1990,
        pages = 2)
  })

  questions_resolved_analysis_binary_offset <-
    MetaculR_analysis_binary_resolved(
      questions_resolved_offset)

  brier_resolved_offset_notme_all_question <-
    MetaculR_brier(
      questions_resolved_analysis_binary_offset %>%
        dplyr::filter(id %in% c(9933, 9939, 10282, 9790)), ### Track Record: all times, 2022-02-01 - 2022-04-15; resolve time, 2022-03-15 - 2022-04-15; close time, 2022-03-22 - 2022-04-11 [9609 has no "Me" prediction]
      me = FALSE,
      time = "all",
      unit = "second")

  testthat::expect_equal(
    brier_resolved_offset_notme_all_question$brier_me$bs,
    NA)
  testthat::expect_equal(
    round(brier_resolved_offset_notme_all_question$brier_Metaculus$bs, 3),
    0.069)
  testthat::expect_equal(
    round(brier_resolved_offset_notme_all_question$brier_community$bs, 3),
    0.073)
})
