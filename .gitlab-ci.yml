image: docker:stable

variables:
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_DRIVER: overlay2
  IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  # Set to "yes" to push image to container registry https://registry.gitlab.com/group/project/image (see https://gitlab.com/USERname/PROJECTname/container_registry)
  PUSH_IMAGE_TO_REGISTRY: "no"
  # Set to "false" to release v1.0.0 and exit initial development phase
  GSG_INITIAL_DEVELOPMENT: "true"

# Don't forget to
  # - Create a personal access token with api scope
  # - Assign it to `GL_TOKEN` in your project ‘Settings → CI/CD → Variables’


stages:
  - version
  - build
  - test
  - deploy

services:
  - docker:dind

calculate-next-version:
  stage: version
  image: registry.gitlab.com/juhani/go-semrel-gitlab:v0.21.0
  script:
    - release next-version --allow-current > .next-version
  artifacts:
    paths:
      - .next-version
  except:
    - tags

compile:
  stage: build
  image: registry.gitlab.com/juhani/go-semrel-gitlab:v0.21.0
  script:
  - release test-git --list-other-changes || true
  - release test-api
  - release -v
  - release help
  - echo "RELEASE_URL=https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/jobs/$CI_JOB_ID/artifacts/release" > build_info
  - echo "RELEASE_DESC=\"Linux amd64 binary\"" >> build_info
  - echo "RELEASE_SHA=$CI_COMMIT_SHA" >> build_info
  - echo "RELEASE_VERSION=$(<.next-version)" >> build_info
  artifacts:
    paths:
    #- release
    - build_info
    #- gendoc
    #- build
  only:
    variables:
        - $GL_TOKEN
  except:
  - tags

verify-release-details:
  stage: test
  image: registry.gitlab.com/juhani/go-semrel-gitlab:v0.21.0
  script:
    - rm -f release_info
    - mv build_info release_info
    - . release_info
    - release next-version --allow-current
    - release test-git
  only:
    variables:
        - $GL_TOKEN

# check-package-base:
#   stage: test
#   image: rocker/r-base
#   script:
#     #- sudo apt-get update; sudo apt-get install graphviz -y
#     - apt-get update
#     - apt-get install --yes --no-install-recommends r-cran-testthat r-cran-devtools
#     - R -e "devtools::install_deps(dependencies = TRUE)"
#     - R -e "devtools::check(vignettes = FALSE)"
#   only:
#     - dev

check-package-devel:
  stage: test
  image: rocker/r-devel
  script:
    #- sudo apt-get update; sudo apt-get install graphviz -y
    - apt-get update
    - apt-get install --yes --no-install-recommends r-cran-testthat r-cran-devtools pandoc
    - R -e "devtools::install_deps(dependencies = TRUE)"
    # - R -e "devtools::check(vignettes = FALSE)"
    - R -e "devtools::check_win_devel(quiet = TRUE)"
  only:
    - dev

# To have the coverage percentage appear as a gitlab badge follow these
# instructions:
# https://docs.gitlab.com/ee/user/project/pipelines/settings.html#test-coverage-parsing
# The coverage parsing string is
# Coverage: \d+\.\d+

code-coverage:
  stage: test
  image: rocker/r-base
  allow_failure: true
  when: on_success
  only:
    - master
  script:
    - apt-get update
    - apt-get install --yes --no-install-recommends r-cran-testthat r-cran-devtools pandoc libssl-dev libcurl4-openssl-dev
    - R -e "devtools::install_deps(dependencies = TRUE)"
    - R -e 'install.packages("covr")'
    - R -e 'install.packages("DT")'
    - R -e 'covr::gitlab(file = "coverage/coverage.html", quiet = FALSE)'
  coverage: '/Coverage: \d+\.\d+/'
  artifacts:
    paths:
      - coverage

# Enable the Container Registry for your GitLab instance (default is enabled for gitlab.com),
# then uncomment script in push-image-to-registry job (this may only work on public projects?)
# https://docs.gitlab.com/ee/user/project/container_registry.html
# Once enabled for your GitLab instance, to enable Container Registry for your project (default is enabled for gitlab.com once done for another project?):
#    Go to your project’s Settings > General page.
#    Expand the Visibility, project features, permissions section and enable the Container Registry feature on your project. For new projects this might be enabled by default. For existing projects (prior GitLab 8.8), you will have to explicitly enable it.
#    Press Save changes for the changes to take effect. You should now be able to see the Packages > Container Registry link in the sidebar.
push-image-to-registry:
  before_script:
    - docker info
  stage: deploy
  only:
    refs:
      - master
    variables:
      - $PUSH_IMAGE_TO_REGISTRY == "yes"
  script:
    - ls -al
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG

publish-release-details:
  stage: deploy
  image: registry.gitlab.com/juhani/go-semrel-gitlab:v0.21.0
  script:
    - rm -f release_info
    - mv build_info release_info
    - . release_info
    - release -v
    - release changelog -f NEWS.md
    - release commit-and-tag NEWS.md release_info
    #- release --ci-commit-tag v$RELEASE_VERSION add-download-link -n release -u $RELEASE_URL -d "$RELEASE_DESC"
  when: manual
  only:
    - master

# To produce a code coverage report as a GitLab page see
# https://about.gitlab.com/2016/11/03/publish-code-coverage-report-with-gitlab-pages/
pages:
  stage: deploy
  dependencies:
    - code-coverage
  script:
#    - R -e "pkgdown::build_site()"
    - mkdir .public
    - cp -r docs/* .public
    - cp -r coverage/* .public
    - mv .public public
  artifacts:
    paths:
      - public
    expire_in: 30 days
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
