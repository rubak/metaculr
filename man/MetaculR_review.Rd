% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/basics.R
\name{MetaculR_review}
\alias{MetaculR_review}
\title{Systematically review your predictions}
\usage{
MetaculR_review(MetaculR_questions_open = NULL, csrftoken = NULL, offset = 0)
}
\arguments{
\item{MetaculR_questions_open}{MetaculR_questions object of your open questions.}

\item{csrftoken}{The csrftoken returned by \code{MetaculR_login()}}

\item{offset}{An offset to start at question 8/47 if you've already reviewed questions 1 - 7.}
}
\value{
Plots
}
\description{
This currently only works for binary questions.
}
\examples{
\dontrun{
questions_recent_open <-
  MetaculR_questions(order_by = "close_time",
                     status = "open",
                     guessed_by = MetaculR_response_login$Metaculus_user_id)

MetaculR_review(questions_recent_open,
  MetaculR_response_login$csrftoken)
}
}
