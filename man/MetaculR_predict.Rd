% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/basics.R
\name{MetaculR_predict}
\alias{MetaculR_predict}
\title{Make predictions via Metaculus API}
\usage{
MetaculR_predict(
  api_domain = "www",
  Metaculus_id = NULL,
  prediction = NULL,
  csrftoken = NULL
)
}
\arguments{
\item{api_domain}{Use "www" unless you have a custom Metaculus domain}

\item{Metaculus_id}{The ID of the question to predict}

\item{prediction}{Your new prediction for the question, e.g., \code{.25} or \code{1:3}}

\item{csrftoken}{The csrftoken returned by \code{MetaculR_login()}}
}
\value{
API response
}
\description{
Make predictions via Metaculus API
}
\examples{
\dontrun{
Metaculus_response_login <- MetaculR_login()

MetaculR_predict(
  Metaculus_id = 10004,
  prediction = 0.42, # prediction = "42:58"
  csrftoken = Metaculus_response_login$csrftoken)
}

}
